#include <stdio.h>
#include "queue.h"

int main(void)
{
    queue_t q;
    init(&q);

    for(int i = 0; i < 10; i++){
        enqueue(&q, i);
        print_queue(&q);
        printf("%d\n", get_sum(&q, 2, 9));
    }

    return 0;
}
