#include <stdio.h>
#include "queue.h"

void init(queue_t *q)
{
    for(int i = 0; i < N; i++){
        q->data[i] = 0;
    }
    q->head = 0;
    q->size = 0;
}

void enqueue(queue_t *q, int n)
{
    q->data[(q->head + q->size) % N] = n;
    if(q->size < N){
        q->size++;
    }else{
        q->head = (q->head + 1) % N;
    }
}

void print_queue(queue_t *q)
{
    for(int i = 0; i < N; i++){
        printf("%d,", q->data[(q->head + i) % N]);
    }
    printf("\n");
}

int get_sum(queue_t *q, int start, int stop)
{
    int max = stop - start;
    int sum = 0;
    for(int i = 0; i < max; i++){
        sum += q->data[(q->head + start + i) % N];
    }
    return sum;
}
