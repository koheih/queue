#define N 10

typedef struct queue {
    int data[N];
    int head;
    int size;
} queue_t;

void init(queue_t *q);
void enqueue(queue_t *q, int n);
void print_queue(queue_t *q);
int get_sum(queue_t *q, int start, int stop);
